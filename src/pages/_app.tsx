import Footer from "@/components/footer/Footer";
import Header from "@/components/header/Header";
import "@/styles/globals.css";
import '../../node_modules/bootstrap/dist/css/bootstrap.css';
import '../styles/Category.module.css'
import type { AppProps } from "next/app";
import BootstrapJs from "@/components/bootstrap-js/BootstrapJs";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Header/>
      <Component {...pageProps} />
      <BootstrapJs/>
      <Footer/>
    </>
  )
}
