"use client"
import React from 'react'
import ProductDetailCard from '@/components/productDetailCard/ProductDetailCard';
import { useRouter } from 'next/router';


const ProductDetail = (props:any) => {

  const route = useRouter();
  const {productDetails} = route.query; 

  return (
    <>
        <ProductDetailCard productId={productDetails}/>
    </>
  )
}

export default ProductDetail