import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import Category from "@/components/category/Category";
import Banner from "@/components/banner/Banner";
import TodaysDeal from "@/components/todaysDeal/TodaysDeal";
import RecommendedItems from "@/components/recommendedItems/RecommendedItems";
import OfferSection from "@/components/OfferSection/OfferSection";
import OtherOffers from "@/components/OtherOffers/OtherOffers";
import OtherAdds from "@/components/otherAdds/OtherAdds";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Category/>
      <Banner/>
      <TodaysDeal/>
      <RecommendedItems/>
      <OfferSection/>
      <OtherOffers/>
      <OtherAdds/>
    </>
  );
}
